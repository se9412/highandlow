package HighAndLow;

public class Card {

	//カードの数字
	//カードの絵柄(?)
	private int[] rank = {1,2,3,4,5,6,7,8,9,10,11,12,13};
	private String[] suit = {"club", "diamond", "heart", "spade", "joker"};


	//getter/setter
	public int[] getRank() {
		return rank;
	}

	public void setRank(int[] rank) {
		this.rank = rank;
	}

	public String[] getSuit() {
		return suit;
	}

	public void setSuit(String[] suit) {
		this.suit = suit;
	}


}
