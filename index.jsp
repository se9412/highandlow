<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="HighAndLow.Card" import="HighAndLow.Deck" import="HighAndLow.Player" %>

<%
	//インスタンスの生成
	Deck deck = new Deck();
	Player player = new Player();
	Player cpu = new Player();
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>High&Low</title>
</head>
<body>
	<article>
		<section class="gametitle">
			<h1>High&Low</h1>
		</section>
		<section class="gamerule">
			<h2>ルール説明</h2>
			<p>1枚のカードが公開されています。<br>
			これから引くカードの数字がそのカードの数字よりも、<br>
			大きいか小さいかを当てるゲームです。<br>
			High(大きい)かLow(小さい)のボタンをクリックするとカードをめくることができます。<br>
			(ボタンの下に結果が表示されます。)</p>
		</section>
		<section class="game">
			<ul class="game__cardList" style="list-style-type: none;">
				<li class="game__cardList--1stItem">
					<h3>このカードより大きいか小さいか</h3>
					<!-- 表向きで表示される比較対象のカード -->
					<img src="trump_images/card_<%=cpu.Draw()%>.png" alt="1st Card" width="150px" height="220px">
				</li>
				<li class="game__cardList--2ndItem">
					<h3>あなたのカード</h3>
					<!-- 裏向きで表示される比較するカード。ボタンのクリックで表向きになる -->
					<img src="trump_images/card_back.png" alt="2nd Card" id="2ndCard" width="150px" height="220px">
				</li>
			</ul>
		</section>
		<!-- HighかLowか選択 -->
		<section class="button">
			<input type="button" value="High" onclick="ImgChange();High();Decision();" id="High">
			<input type="button" value="Low" onclick="ImgChange();Low();Decision();" id="Low">
		</section>
		<!-- 結果を表示させるスペース -->
		<section id="result">
		</section>
		<!-- ページを更新させる-->
		<input type="button" value="もう一度挑戦する" onclick="Reloading()">
	</article>
	<script>
		//画像を格納する配列
		var img = new Array();

		//画像を格納
		img[0] = new Image();
		img[0].src = "trump_images/card_<%=player.Draw()%>.png";

		//pタグを追加する位置を決めるのに使用する
		var target = document.getElementById("result");


		//HighかLowのボタンを押すとid"2ndCard"をimg[0]の画像に切り替える関数
		function ImgChange(){
			document.getElementById("2ndCard").src = img[0].src;
		}

		//Highのボタンを押したときに表示される
		function High(){
			var pTag = document.createElement("p");
			pTag.setAttribute("id", "resultHigh");
			pTag.innerHTML = "あなたはHighを選択しました。";
			target.appendChild(pTag);
		}

		//Lowのボタンを押したときに表示される
		function Low(){
			var pTag = document.createElement("p");
			pTag.setAttribute("id", "resultLow");
			pTag.innerHTML = "あなたはLowを選択しました。";
			target.appendChild(pTag);
		}

		//自分のカードが公開された時のHighかLowかの判定
		//押したボタンに対応しての勝ち負けの判定
		function Decision(){
			//2枚のカードの得点
			var cpuScore = <%=cpu.getScore()%>;
			var playerScore = <%=player.getScore()%>;
			//pタグの生成
			var pTag = document.createElement("p");
			//押したボタンの取得
			var reHi = document.getElementById("resultHigh");
			var reLo = document.getElementById("resultLow");
			//どちらの得点が大きいか、どちらのボタンを押したのか
			if(cpuScore > playerScore){
				if(reHi != null){
					pTag.innerHTML = "結果：残念...Low(あなたの選んだカードの数字の方が小さい)でした";
				} else if(reLo != null){
					pTag.innerHTML = "結果：おめでとう！Low(あなたの選んだカードの数字の方が小さい)でした！";
				}
			} else if(cpuScore < playerScore){
				if(reHi != null){
					pTag.innerHTML = "結果：おめでとう！High(あなたの選んだカードの数字の方が大きい)でした！";
				} else if(reLo != null){
					pTag.innerHTML = "結果：残念...High(あなたの選んだカードの数字の方が大きい)でした";
				}
			} else if(cpuScore == playerScore){
				pTag.innerHTML = "結果：引き分けです";
			}
			target.appendChild(pTag);
		}


		//もう一度挑戦するボタンを押すとページを更新するための関数
		function Reloading(){
			location.reload();
		}
	</script>
</body>
</html>