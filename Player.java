package HighAndLow;

public class Player {

	//カードの数字を格納する変数
	private int score;


	//カードを引くためのメソッド
	//deckリストの0番目を取得し、deckリストから削除する
	//deckPointリストの0番目をscoreに代入し、deckPointリストから削除する
	//戻り値はdeckリストの0番目を取得したdrawCard
	public String Draw(){
		String drawCard = Deck.deck.get(0);
		Deck.deck.remove(0);
		score = Deck.deckPoint.get(0);
		Deck.deckPoint.remove(0);
		return drawCard;
	}


	//getter/setter
			public int getScore() {
				return score;
			}

			public void setScore(int score) {
				this.score += score;
			}

}
