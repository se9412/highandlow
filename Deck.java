package HighAndLow;
import java.util.LinkedList;

public class Deck {
	//山札とカードそれぞれの数字を格納するリスト
	static LinkedList<String> deck = new LinkedList<String>();
	static LinkedList<Integer> deckPoint = new LinkedList<Integer>();

	Card card = new Card();


	//山札(deck)を生成し、シャッフルするコンストラクタ(画像のリンクの一部として山札に格納)
	public Deck(){
		for(int i = 0; i < card.getSuit().length; i++){
			//getSuit()がjokerのとき
			if(card.getSuit()[i].equals("joker")){
				deck.add(card.getSuit()[i]);
				deckPoint.add(14);
			} else {
				//jokerではないとき
				for(int j = 0; j < card.getRank().length; j++){
					deck.add(card.getSuit()[i] + "_" + card.getRank()[j]);
					deckPoint.add(card.getRank()[j]);
				}
			}
		}
//		山札をシャッフル(fisherYatesアルゴリズム)
		for(int k = deck.size() - 1; k > 0; k--){
			int l = (int) Math.floor(Math.random() * (k + 1));
			String tmp = deck.get(k);
			deck.set(k, deck.get(l));
			deck.set(l, tmp);
			int tmpPoint = deckPoint.get(k);
			deckPoint.set(k, deckPoint.get(l));
			deckPoint.set(l, tmpPoint);
		}
	}
}
